(c) 2016 Mimir Consulting LLC

Apollo: Greek god of knowledge, light, and prophecy (Also the moon landing program).

In its current form, this maven project is a proof-of-concept of a data processor dependant on the no.priv.garshol.duke
and com.thinkaurelius.titan packages.

This project is most compatible with the "IntelliJ IDEA" IDE

To Compile (skipping tests): 
    
    in root dir, $ mvn clean install -DskipTests

To Run:

    in root dir, $ mvn exec:java -Dexec.mainClass=mimir.DukeLoader


NOTE:

    - There must be a cassandra server running locally with thrift enabled (start_rpc: true) in the cassandra.yaml file
      for tests to pass.

    - If two records have the same ID, Duke does not return the records as matching because a record will not return as
      matching to itself.
      
    - In the config.xml file, Low probability = probability when values don't match. High = probability when they do
      match.


TESTING:
    
    test classes in root/src/test/java

    run all unit tests:   $ mvn test
    run single test:      $ mvn test -Dtest=FooBar
