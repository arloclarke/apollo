package mimir;

import com.thinkaurelius.titan.core.TitanFactory;
import com.thinkaurelius.titan.core.TitanGraph;
import com.thinkaurelius.titan.core.schema.TitanManagement;
import com.thinkaurelius.titan.graphdb.configuration.GraphDatabaseConfiguration;
import com.thinkaurelius.titan.core.TitanVertex;
import org.apache.commons.configuration.BaseConfiguration;
import org.apache.commons.configuration.Configuration;
//import org.json.simple.JSONObject;
import org.elasticsearch.search.aggregations.metrics.percentiles.InternalPercentileRanks;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.JSONException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.io.FileReader;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Created by ian on 12/10/15. THIS IS A PROOF OF CONCEPT. Not for production.
 */
public class LoadData {

    TitanGraph g;
    TitanManagement m;

    public LoadData() {

//        g = TitanFactory.open("/Users/ian/Desktop/Mimir Benchmarks/titan.properties");
        g = TitanFactory.build().set("storage.backend", "cassandrathrift")
               // .set("storage.batch-loading", "true")
               // .set("schema.default", "default") // TODO: Take this out in production (Turn off automatic schema creation)
                .open();
       // int i = 0;
       // for (TitanVertex v : g.query().vertices()){
       //     i++;
       // }
       // System.out.println(i);
        // g = TitanFactory.open("/Users/arloclarke/Desktop/Mimir/duke/dedup/titan.properties");

        m = g.openManagement();

        m.getOrCreateVertexLabel("models.Person");

        m.commit();

//        g.getOrCreateVertexLabel("")
    }

    /***
     * Loads input from XML parser into the Mimir Models we will eventually make to represent nodes.
     * @throws JSONException
     */
    public void hello_word(String file_name) throws JSONException, FileNotFoundException, ParseException, IOException{
//        g.addVertex("what?");
//        System.out.println(g.query().vertices());
//        System.out.println("Hello, World!");
//        System.out.println(g.toString());

        // A list of documents. Each document is a dictionary
        JSONParser parser = new JSONParser();
        FileReader file_reader = new FileReader(file_name);
        JSONArray document_array = (JSONArray)parser.parse((String)parser.parse(file_reader)); // Ew

        //System.out.println("\nHERE\n" + document_array);


        for (int i = 0 ; i < document_array.size() ; i++) {
            JSONObject document = (JSONObject) document_array.get(i);

            for (Object obj : document.keySet()) {
                String key = (String)obj;

                try{ // There has got to be a better way to do this
                    JSONArray doc_entries = (JSONArray)document.get(key);

                    for (int c = 0 ; c < doc_entries.size() ; c++) {
                        create_vertex(key, (JSONObject)doc_entries.get(c)).label();
                    }

                } catch (ClassCastException e) {
                    create_vertex(key, (JSONObject)document.get(key)).label();
                }
                // Make vertex of type key here.
                // connect

            }

        }
        file_reader.close();

    }

    public TitanVertex create_vertex(String type, JSONObject contents) throws JSONException {
        TitanVertex new_vertex = g.addVertex(type);

        for (Object obj : contents.keySet()) {
            String key = (String) obj;
            new_vertex.property(key, (String) contents.get(key));
        }
        return new_vertex;
    }

}
