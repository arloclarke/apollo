package mimir;

import no.priv.garshol.duke.*;
import no.priv.garshol.duke.matchers.PrintMatchListener;

public class DukeLoader {

  public static void main(String[] argv) throws Exception {
    Configuration config = ConfigLoader.load("duke_config.xml");
    Processor proc = new Processor(config);
    proc.addMatchListener(new PrintMatchListener(true, true, true, false,
                                                config.getProperties(),
                                                true));
    proc.deduplicate();
    proc.close();
  }
}