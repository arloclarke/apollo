package mimir;

import no.priv.garshol.duke.DataSource;
import no.priv.garshol.duke.Logger;
import no.priv.garshol.duke.RecordIterator;
import com.thinkaurelius.titan.core.TitanGraph;

public class TitanInterface implements DataSource {

    TitanGraph g;

    public TitanInterface(TitanGraph graph) {
        g = graph;
    }


    public RecordIterator getRecords() {

        return new TitanRecordIterator(g);
    }

    public void setLogger(Logger logger) {
        // Implement in the future maybe
    }
}