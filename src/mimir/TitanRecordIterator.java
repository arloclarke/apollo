package mimir;

import com.thinkaurelius.titan.core.TitanVertex;
import com.thinkaurelius.titan.core.TitanGraph;
import no.priv.garshol.duke.Record;



public class TitanRecordIterator extends no.priv.garshol.duke.RecordIterator {
    TitanGraph g;

    TitanRecordIterator(TitanGraph graph) {

        g = graph;

    }

    // DFS of Titan graph
    public Record next()  {
        return null;
    }

    public boolean hasNext() {
        return false;
    }

    public void batchProcessed() {

    }

    public void close() {

    }

    public void remove() {
        
    }
}