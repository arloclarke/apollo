package mimir;

import org.json.JSONException;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.apache.log4j.Logger;
import org.apache.log4j.LogManager;
import org.apache.log4j.Level;
import java.util.List;
import java.util.Collections;
import junit.framework.Assert;
import org.junit.Test;


/**
 * Created by ian on 12/10/15.
 */
public class TestTitan {

    //TODO: this is no bueno test, use something like Assert.fail(LoadData.hello_word("hi"));
    //@Test   (UNCOMMENT FOR TEST)
    public void crude_test_titan() throws JSONException, FileNotFoundException, IOException, ParseException {

        List<Logger> loggers = Collections.<Logger>list(LogManager.getCurrentLoggers());
        loggers.add(LogManager.getRootLogger());
        for ( Logger logger : loggers ) {
            logger.setLevel(Level.OFF);
        }

        LoadData d = new LoadData();

        File folder = new File("src/test/test_data/test_articles/");
        File[] listOfFiles = folder.listFiles();

        long startTime = System.nanoTime();
        long endTime;

        int docs = 0;

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                d.hello_word("src/test/test_data/test_articles/" + listOfFiles[i].getName());
            } else if (listOfFiles[i].isDirectory()) {
                System.out.println("Directory " + listOfFiles[i].getName());
            }
            if (docs == 100) {
                endTime = System.nanoTime();
                long duration = (endTime - startTime);

                System.out.println("Indexed " + i + " documents in " + duration  / 1000000000.0 + " seconds");

                startTime = System.nanoTime();
                docs = 0;

            }
            docs++;
        }

    }
}
